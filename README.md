# VPC Subnet module

Terraform module to provision public and private [`subnets`](http://docs.aws.amazon.com/AmazonVPC/latest/UserGuide/VPC_Subnets.html) in an existing [`VPC`](https://aws.amazon.com/vpc)

__Note:__ this module is intended for use with an existing VPC and existing Internet Gateway.
To create a new VPC, use [vpc](https://gitlab.com/nutellino-playground/tf-modules/vpc.git) module.



## Usage

```hcl
module "subnets" {
  source              = "git::https://gitlab.com/nutellino-playground/tf-modules/vpc-subnet.git?ref=0.0.1"
  namespace           = "cp"
  stage               = "prod"
  name                = "app"
  region              = "us-east-1"
  vpc_id              = "vpc-XXXXXXXX"
  igw_id              = "igw-XXXXXXXX"
  cidr_block          = "10.0.0.0/16"
  availability_zones  = ["us-east-1a", "us-east-1b"]
}
```


